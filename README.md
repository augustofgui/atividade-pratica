# TerraLAB
## Atividade Prática - Sprint 1
##### por Augusto Ferreira Guilarducci

---

### Questões Teóricas

> **1. O que é Git?** \
> **R:** Ele é um sistema de controle de versões, amplamente utilizado para manter e gerenciar bases de código de uma aplicação e suas mudanças ao longo do desenvolvimento.

> **2. O que é a staging area?** \
> **R:** É o conjunto de arquivos que serão incluidos no próximo commit.

> **3. O que é o working directory?** \
> **R:** É o conjunto de arquivos do repositório, no sistema de arquivos local.

> **4. O que é um commit?** \
> **R:** É o estado atual da staging area no momento de criação do commit. Ele armazena as alterações que ocorreram até esse ponto, permitindo que sejam guardados os conteúdos dos arquivos e também suas alterações entre versões.

> **5. O que é uma branch?** \
> **R:** É uma ramificação de uma base de código, criando no sistema de controle de versões uma bifurcação após um commit. Isso permite que um código posso receber e armazenar diferentes alterações, sem que uma possa interferir com a outra, em um primeiro momento. Do mesmo modo, é possível juntar essas branches, sendo necessário lidar com os conflitos entre as modificações, realizando assim um merge.

> **6. O que é o head no Git?** \
> **R:** É um ponteiro para um commit específico.

> **7. O que é um merge?** \
> **R:** É a junção de duas branches diferentes, integrando as modificações de ambas ramificações em uma só. Importante notar que nada deixa de existir, é simplesmente adicionado ao histórico de mudanças da branch que recebe o merge as alterações que ocorreram no segundo branch. Caso ocorra de um trecho ter sido alterado em ambas branches, o merge não é possível, sendo necessário resolver o conflito antes de finalizar a ação.

> **8. Explique os 4 estados de um arquivo no Git.** \
> **R:** Um arquivo do Git pode ter esses 4 estados a seguir:
> - untracked: Arquivos novos ou que não estavam presentes no último commit.
> - unmodified: Arquivos do último commit que não sofreram alteração.
> - modified: Arquivos do último commit que sofreram alteração, porém não foram adiconados ao staging area para o próximo commit.
> - staged: Arquivos que sofreram alteração, ou foram criados, e foram adicionados ao staging area para serem adicionados ao próximo commit.

> **9. Explique o comando git init.** \
> **R:** Ele cria um repositório vazio do Git na pasta em que o comando foi iniciado, caso este repositório não exista. Uma pasta chamada ".git" é criada e é adicionada a ela as configurações iniciais do repositório.

> **10. Explique o comando git add.** \
> **R:** Ele adiciona o(s) arquivo(s), que são untracked ou modified, para a staging area.

> **11. Explique o comando git status.** \
> **R:** Ele exibe o estado atual do working directory, incluindo quais arquivos estão em quais estados, e quais as mudanças serão incluidas no próximo commit, naquele momento.

> **12. Explique o comando git commit.** \
> **R:** Ele adiciona as mudanças atuais no repositorio, salvando o estado atual das modifições no staging area.

> **13. Explique o comando git log.** \
> **R:** Ele exibe os registros log dos commits, daquele repositório Possibilitando ver a mensagem do commit, sua data de criação, o autor e até as diferenças armazenadas por ele.

> **14. Explique o comando git checkout -b.** \
> **R:** O comando altera o working tree atual para uma branch pré existente, porém a flag "-b" indica que essa branch ainda não existe e, se for mesmo o caso, ela deve ser criada.

> **15. Explique o comando git reset e suas três opções.** \
> **R:** O comando git reset tem três opções:
> - soft: Move o HEAD para o commit indicado, porém mantendo o estado atual da staging area e working tree, não perdendo alterações.
> - hard: Move o HEAD para o commit indicado, porém altera a staging area e working tree para o estado do commit em questão, perdendo alterações.
> - mixed: Move o HEAD para o commit indicado, altera a staging area e porém mantém a working tree, não perdendo alterações mas remove os arquivos do estado de staged, reiniciando o próximo commit.

> **16. Explique o comando git revert.** \
> **R:** Ele reverte as alterações de um commit indicado criando um novo commit que desfaz as modificações salvas no commit indicado.

> **17. Explique o comando git clone.** \
> **R:** Clona o repositório online para um novo diretório na máquina local.

> **18. Explique o comando git push.** \
> **R:** Ele atualiza o repositório remoto com as mudanças dos novos commits e os arquivos associados.

> **19. Explique o comando git pull.** \
> **R:** Ele atualiza o repositório local com as mudanças novas que existem no repositorio remoto e os arquivos associados.

> **20. Como ignorar o versionamento de arquivos no Git?** \
> **R:** Podemos adicionar o caminho do(s) arquivo(s) e/ou pasta(s) que queremos ignorar a um linha do arquivo de configuração chamado ".gitignore", criado na raiz do projeto. Ele deve ser adicionado ao staging area e commitado como qualquer outro arquivo para que possa existir em todos os repositorios, clonados a partir do remoto.

> **21. No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.** \
> **R:** 
> - master: é a branch principal do projeto, de onde se origina as outras. Ela tem o objetivo de servir como a versão atual do projeto, contendo o código da versão que estiver em ação no mercado.
> - develop: é a branch ramificada a partir da master e tem como objetivo servir de área para novas modificações e melhoras/correções, simulando a branch atual de master. Assim, pode se testar a integração de novas funcionalidades a base de código e suas consequências.
> - staging: é um branch feita a partir da develop para que seja possível testar as modificações em uma bateria de teste, idealmente automatizados, permitindo que um merge com a master seja possível sem erros prévios. Isso gera uma nova versão do software, atualizando a master, gerando um ciclo onde a develop deve ser atualizada a partir da master e o staging a partir da develop. Esse fluxo permite que atualizações ao software possam ocorrer sem impedimentos, tanto no uso pelo consumidor mas também no lado do desenvolvedor, que pode aderir novas funcionalidades sem medo.

---
 \
### Questões Práticas

[Link do repositório](https://gitlab.com/augustofgui/atividade-pratica)

> **1. A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.**

> **2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o [artigo](https://raullesteves.medium.com/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8) como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão:**
> - README.md onde o trainne irá continuamente responder as perguntas em formas de commit.
> - Inserção de código, exemplo de commit de feature:
>   - *git commit -m "{PERGUNTA}" -m "{RESPOSTA}"*

> **3. Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:**
> ```js
>const args = process.argv.slice(2);
>console.log(parseInt(args[0]) + parseInt(args[1]));
> ```
> **Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:** \
> **R:** Para executa-lo deve utilizar o comando a seguir: \
> `node calculadora.js a b`
> 
> Sendo "a" e "b" dois números a escolha. O código pega estes números a partir do array de argumentos e imprime na saida padrão a soma de ambos.

> **4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.** \
> **R:** Para subi-lo deve utilizar o comando a seguir: \
> `git add calculadora.js`
> 
> **Que tipo de commit esse código deve ter de acordo ao conventional commit.** \
> **R:** feat: MENSAGEM
> 
> **Que tipo de commit o seu README.md deve contar de acordo ao conventional commit.** \
> **R:** docs: MENSAGEM
>
> **Por fim, faça um push desse commit.**

> **5. Copie e cole o código abaixo em sua calculadora.js:**
> ```js
>const soma = () => {
>   console.log(parseInt(args[0]) + parseInt(args[1]));
>};
> 
>const args = process.argv.slice(2);
> 
>soma();
> ```
> **Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.** \
> **R:** refactor: MENSAGEM

> **6. João entrou em seu repositório e o deixou da seguinte maneira:**
> ```js
>const soma = () => {
>    console.log(parseInt(args[0]) + parseInt(args[1]));
>};
>
>const sub = () => {
>    console.log(parseInt(args[0]) - parseInt(args[1]));  
>}
>
>const args = process.argv.slice(2);
>
>switch (args[0]) {
>    case 'soma':
>        soma();
>    break;
>
>    case 'sub':
>        sub();
>    break;
>    
>    default:
>        console.log('does not support', args[0]);
>}
> ```
> **Depois disso, realizou um git add . e um commit com a mensagem: "Feature: added subtraction"**
>
> **Faça como ele e descubra como executar o seu novo código.** \
> **R:** `node calculadora.js operacao a b`
>
> **Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e <s>divisão</s> subtração funcionem.** \
> **R:** Eram dois erros:
> 1. Em ambas as funções, `soma()` e `sub()`, os número estavam nos indexes errados do array `args`. Eles estavam como `parseInt(args[0]) + parseInt(args[1])`, porém deveriam ser `parseInt(args[1]) + parseInt(args[2])` já que `args[0]` é a operação.
> 2. No bloco default do switch o array estava escrito como `arg[0]` enquanto que deveria ser `args[0]`
>
> **Por fim, commit sua mudança.** \
> **R:** fix: MENSAGEM

> **7. Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.**
>
> **Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.** \
> **R:** Para criar uma nova branch deve se usar o comando:
> `git checkout -b NOME_BRANCH`

> **7. Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.**
>
> **Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.** \
> **R:** Para criar uma nova branch deve se usar o comando:
> `git checkout -b NOME_BRANCH`

> **8. Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request.** \
> **Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.** \
> **R:** Criar branch de staging a partir da develop, testar o código nesta nova branch e realizar o merge com a main/master. Deve atualizar a develop com a alteração nova na main.

> **9. João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, fez a seguinte alteração no código e fez o commit com a mensagem:** \
> `feat: add conditional evaluation`
> ```js
>var x = args[0];
>var y = args[2];
>var operator = args[1];
>
>function evaluate(param1, param2, operator) {
>  return eval(param1 + operator + param2);
>}
>
>if ( console.log( evaluate(x, y, operator) ) ) {}
>```
> **Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!**

> **10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.** \
> **R:** O código de joaozinho utiliza da função `eval()` que recebe como parâmetro uma String e, caso ela seja uma expressão aritmética, ela a resolve retornando o valor esperado pela expressão. Porém, para executar esse código, não se deve utilizar o nome da expressão e sim seu símbolo. EX: "+", "-", "/", etc... \
> Desse modo, a execução do código deveria ser feita assim: \
> `node calculadora.js a operacao b` \
> Sendo "a" e "b" números e "operacao" um símbolo operador.